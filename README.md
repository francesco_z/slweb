# slweb - [Bootstrap](http://getbootstrap.com/)

Static website, customized according to SL requests.
LESS CSS dynamic preprocessor, Gulp code minifyer and package.json dependencies file are not used.
Created from [Bootstrap](http://getbootstrap.com/) template [Creative](http://startbootstrap.com/template-overviews/creative/), with the addition of a slideshow functionality of [FadeShow](http://terwanerik.github.io/FadeShow).

## Getting Started

To begin using this template:
* Clone the repo: `git clone https://gitlab.com/francesco_z/slweb.git`

## Creator

This repository was created and entirely maintained by **[Francesco Zaccaria](https://www.linkedin.com/in/francesco-zaccaria-3b038a10a/)**, only member of Z-studio.

* https://gitlab.com/francesco_z

## Copyright and License

Copyright 2019 Z-Studio. Code released under the [MIT](https://gitlab.com/francesco_z/slweb/blob/master/LICENSE) license.
